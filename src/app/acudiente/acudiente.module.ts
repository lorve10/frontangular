import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AcudienteRoutingModule } from './acudiente-routing.module';
import { IndexComponent } from './index/index.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FiltroPipe } from './pipes/filtro.pipe';


@NgModule({
  declarations: [IndexComponent, IndexComponent, CreateComponent, EditComponent,FiltroPipe ],
  imports: [
    CommonModule,
    AcudienteRoutingModule,
    FormsModule,
  ReactiveFormsModule,
  ]
})
export class AcudienteModule { }
