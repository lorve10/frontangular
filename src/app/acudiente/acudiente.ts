export interface Acudiente {

    id: number;
    cedula: number;
    nombre: string;
    apellido: string;
    telefono: string;
    parentesco: string;

}
