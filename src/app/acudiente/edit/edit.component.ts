import { Component, OnInit } from '@angular/core';
import { AcudienteService } from '../acudiente.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Acudiente } from '../acudiente';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  id: number;
  acudi: Acudiente;
  form :FormGroup;


    constructor(
      public acudienteService: AcudienteService,
      private route: ActivatedRoute,

      private router: Router
      ) { }


  ngOnInit(): void {

      this.id = this.route.snapshot.params['idAcudiente'];
      console.log(this.id);

      this.acudienteService.find(this.id).subscribe((data: Acudiente)=>{
      this.acudi = data;
      console.log(data);

      });
      console.log(this.acudi);

      this.form = new FormGroup({
        cedula:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
        nombre:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
        apellido:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
        parentesco:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
        telefono:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
      });

  }
  get f(){
    return this.form.controls;
  }
  submit(){
  console.log(this.form.value);
  this.acudienteService.update(this.id, this.form.value).subscribe(res => {
       console.log('Person updated successfully!');
       this.router.navigateByUrl('acudiente/index');
  })
}

}
