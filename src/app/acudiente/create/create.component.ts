import { Component, OnInit } from '@angular/core';
import { AcudienteService } from '../acudiente.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  form :FormGroup;

  constructor(
    public acudienteService: AcudienteService,
    private router: Router
    ) { }

    ngOnInit(): void {
      this.form = new FormGroup({
        cedula:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
        nombre:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
        apellido:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
        parentesco:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
        telefono:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
      });
    }

    guardar(){
      console.log(this.form.value);

      this.acudienteService.create(this.form.value).subscribe(res=>{
        console.log("respuesta");
        console.log(res);
        console.log("Guardar");
        this.router.navigateByUrl('acudiente/index');




      })
    }
}
