import { Pipe, PipeTransform } from '@angular/core';
import { Acudiente } from '../acudiente';
@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform( acudiente: Acudiente[], search: string = ''): Acudiente[] { /// funcion de filtrado
    console.log(search);
    const filtroLista = acudiente.filter(item => item.nombre.toLowerCase().indexOf(search) !== -1 ||
    JSON.stringify(item.cedula).toLowerCase().indexOf(search) !== -1);
    return filtroLista;

  }

}
