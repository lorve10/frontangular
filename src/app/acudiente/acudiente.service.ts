import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {  Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Acudiente } from './acudiente';

@Injectable({
  providedIn: 'root'
})
export class AcudienteService {
  private apiURL = "http://localhost:8000/api/acudiente/";

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

constructor(private httpClient: HttpClient) { }

getAll(): Observable<Acudiente[]> {
   return this.httpClient.get<Acudiente[]>(this.apiURL)
   .pipe(
     catchError(this.errorHandler)
   )
 }

 create(Acudiente): Observable<Acudiente> {
   return this.httpClient.post<Acudiente>(this.apiURL, JSON.stringify(Acudiente), this.httpOptions)
   .pipe(
     catchError(this.errorHandler)
   )
 }

 find(id): Observable<Acudiente> {
   return this.httpClient.get<Acudiente>(this.apiURL + id)
   .pipe(
     catchError(this.errorHandler)
   )
 }

 consulta(name): Observable<Acudiente> {
   return this.httpClient.post<Acudiente>(this.apiURL, name, this.httpOptions)
   .pipe(
     catchError(this.errorHandler)
   )
 }
 update(id, Acudiente): Observable<Acudiente> {
   return this.httpClient.put<Acudiente>(this.apiURL + id, JSON.stringify(Acudiente), this.httpOptions)
   .pipe(
     catchError(this.errorHandler)
   )
 }

 delete(id){
   return this.httpClient.delete<Acudiente>(this.apiURL + id, this.httpOptions)
   .pipe(
     catchError(this.errorHandler)
   )
 }

 errorHandler(error) {
   let errorMessage = '';
   if(error.error instanceof ErrorEvent) {
     errorMessage = error.error.message;
   } else {
     errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
   }
   return throwError(errorMessage);
 }

}
