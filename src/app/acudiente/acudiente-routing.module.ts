import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexComponent } from './index/index.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  { path: 'acudiente', redirectTo: 'acudiente/index', pathMatch: 'full'},
  { path: 'acudiente/index', component: IndexComponent },
  { path: 'acudiente/create', component: CreateComponent },
  { path: 'acudiente/edit/:idAcudiente', component: EditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcudienteRoutingModule { }
