import { Component, OnInit } from '@angular/core';
import { AcudienteService } from '../acudiente.service';
import { Acudiente } from '../acudiente';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  acudiente: Acudiente[] = [];
  consulta: string;
  public search: string = "";
  public variable: string = "";
  /// creacion varibales

  constructor(public acudienteService: AcudienteService) { } //instaciar c

  ngOnInit(): void {  ///llamndo a la lista de acudientes
    this.acudienteService.getAll().subscribe((data: Acudiente[])=>{
    this.acudiente = data;
    console.log(this.acudiente);
  })
  }

  onSearch( search: string ){ /// buscador filtrando por cedula
  this.search = search;
  }

}
